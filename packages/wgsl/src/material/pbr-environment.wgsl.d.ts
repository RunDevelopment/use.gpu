declare module "@use-gpu/wgsl/material/pbr-environment.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const applyPBREnvironment: ParsedBundle;
  export default __module;
}
