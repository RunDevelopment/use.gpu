declare module "@use-gpu/wgsl/pmrem/pmrem-read.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const sampleEnvMap: ParsedBundle;
  export default __module;
}
