declare module "@use-gpu/wgsl/geometry/normal.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getOrthoVector: ParsedBundle;
  export default __module;
}
