declare module "@use-gpu/wgsl/codec/rgbm.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const decodeRGBM16: ParsedBundle;
  export default __module;
}
