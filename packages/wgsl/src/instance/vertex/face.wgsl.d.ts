declare module "@use-gpu/wgsl/instance/vertex/face.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getFaceVertex: ParsedBundle;
  export default __module;
}
