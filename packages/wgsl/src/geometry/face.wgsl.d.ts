declare module "@use-gpu/wgsl/geometry/face.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getFaceSegment: ParsedBundle;
  export default __module;
}
