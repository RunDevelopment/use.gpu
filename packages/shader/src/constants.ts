export const HASH_KEY = 0x12345678;

export const PREFIX_CAST = '_CT_';
export const PREFIX_CHAIN = '_CH_';
export const PREFIX_CLOSURE = '_CL_';
export const PREFIX_VIRTUAL = '_VT_';

export const VIRTUAL_BINDINGS = '_VT_';