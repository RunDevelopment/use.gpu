declare module "@use-gpu/wgsl/instance/fragment/lit.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLitFragment: ParsedBundle;
  export default __module;
}
